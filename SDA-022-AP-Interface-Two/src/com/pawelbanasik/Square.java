package com.pawelbanasik;

public class Square implements IFigure{
    private double a;

    public Square(double a) {
        this.a = a;
    }

    @Override
    public double countArea(){
        return a * a;
    }

    @Override
    public double countCircumference(){
        return a *4;
    }
}
