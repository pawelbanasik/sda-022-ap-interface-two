package com.pawelbanasik;

public interface IFigure {
    double countArea();
    double countCircumference();
}
