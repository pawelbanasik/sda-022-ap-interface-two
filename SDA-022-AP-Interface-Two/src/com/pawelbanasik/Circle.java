package com.pawelbanasik;

import static java.lang.Math.*;

public class Circle implements IFigure {
    private double r;

    public Circle(double r) {
        this.r = r;
    }
    
    @Override
    public double countArea(){
        return r * r * PI;
    }

    @Override
    public double countCircumference(){
        return 2 * PI * r ;
    }
}
